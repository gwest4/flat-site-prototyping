var fileinclude = require("gulp-file-include");
var markdown = require("markdown");
var del = require("del");
var gulp = require("gulp");


/*
 *  Sources
 */

var sources = {
  html: [
    "src/",             // Start with everything in src/
    "!src/**/_*.html",  // Exclude partial html
    "!src/js",          // Exclude js folder
    "!src/css"          // Exclude css folder
  ],
  js: "src/js/**",
  css: "src/css/**",
  img: "src/img/**"
}
var outputs = {
  html: "dist/",
  js: "dist/js/",
  css: "dist/css/",
  img: "dist/img/"
}


/*
 *  Building
 */

gulp.task("build", ["build:html", "build:code", "build:img"]);

// Flatten, copy html
gulp.task("build:html", ["clean:html"], function() {
    gulp.src(sources.html)
      .pipe(fileinclude({
        basepath: "src/",
        filters: {
          // Add markdown as an available filter
          markdown: markdown.parse
        },
        context: {
          // Defaults (prevents rendering these directives when empty)
          pageStyles: "",
          pageHeaderScripts: "",
          pageFooterScripts: "",
        }
      }))
      .pipe(gulp.dest(outputs.html));
});

// Copy js, css
gulp.task("build:code", ["clean:code"], function() {
    gulp.src(sources.js).pipe(gulp.dest(outputs.js));
    gulp.src(sources.css).pipe(gulp.dest(outputs.css));
});

// Copy img
gulp.task("build:img", ["clean:img"], function() {
    gulp.src(sources.img).pipe(gulp.dest(outputs.img));
});


/*
 *  Cleaning
 */

gulp.task("clean", ["clean:html", "clean:code", "clean:img",]);

gulp.task("clean:html", function() {
    return del("dist/**/*.html");
});

gulp.task("clean:code", function() {
    return del([outputs.js, outputs.css]);
});

gulp.task("clean:img", function() {
    return del([outputs.img]);
});


/*
 *  Default
 */
 
// Watch for changes, only running partial build tasks as needed
gulp.task("default", function() {
    gulp.watch("src/**/*.html", ["build:html"]);
    gulp.watch(["src/**/*.js", "src/**/*.css"], ["build:code"]);
    gulp.watch("src/img/**", ["build:img"]);
});
