# Flat-Site Prototyping

A bare-bones project for rapid prototyping. Get from installation to prototyping in minutes!

* Uses task-running powered by [gulp](https://github.com/gulpjs/gulp)
* Is set up to use HTML partial files, powered by [gulp-file-include](https://github.com/coderhaoxin/gulp-file-include/blob/master/Readme.md)
* Includes a ready-to-run `gulpfile.js` with both all-in-one and piecemeal build tasks
* Suggests a basic `src/` file structure, seperated by `css/`, `js/`, and `img/` directories
* On build, produces a zip-ready `dist/` containing the compiled project

*And that's about it. Anything else you want included is up to you.*

* * *

## Install

### Pre-installation

**Install [node.js](https://nodejs.org/en/) (required)**

1. Download and run the installer for your system
2. Open the command line
2. Verify that you have installed `node` by running `node -v`
3. Verify that you can run `npm` by running `npm -v`

**Install [gulp-cli](https://www.npmjs.com/package/gulp-cli) (required)**

The gulp-cli (Gulp Command Line Interface) allows us to run gulp tasks (gulp is a "[task-runner](https://www.quora.com/What-is-task-runner)") from the command line.

1. Open the command line
2. Run `npm install --global gulp-cli`
3. Verify that you can run `gulp` by running `gulp -v` from the command line

**Enable running gulp from Sublime Text (optional)**

If you use Sublime Text, you can install a package available via Package Control which can be used to run gulp tasks from inside the Sublime editor.

1. Install [Package Control](https://packagecontrol.io/installation) for Sublime
2. Install [Sublime Gulp](https://packagecontrol.io/packages/Gulp) (via Package Control)
    * In the Sublime Text editor, open the command pallette with `Ctrl + Shift + P`
    * Search for and select `Package Control: Install Package`
    * Search for and select `Gulp` by sublime-gulp.nicosantangelo.com
3. Verify that you have installed Sublime Gulp
    * In the Sublime Text editor, open the command pallete with `Ctrl + Shift + P`
    * Search for `Gulp` and verify that options such as `Gulp`, `Gulp: Kill running tasks`, and `Gulp: Run arbitrary task` exist

### Installation

**Note:** This README doesn't cover setting up and running a local server. If you already have a local server/stack installed (eg. http-server, XAMPP), you only have to `build` the project (detailed below) and serve the resulting `dist/` as the root of your website.

**Clone**

Clone the project into a directory of your choosing. If you are using command-based Git, for example, then you could do the following:

1. Open the command line
2. Run `git clone [repo url] my-project`

**Restore node packages with npm**

1. Open the command line
2. `cd` into `my-project/`
3. Run `npm install`
    * **Note:** This will install all node.js dependencies for this project to `my-project/node_modules/`. You can add other node modules via npm, by running `npm install [package]`. Documentation can be found [here](https://docs.npmjs.com/getting-started/installing-npm-packages-locally).

**Build from the command line with gulp**

1. Open the command line
2. `cd` into `my-project/`
3. Run `gulp build`
    * **Note:** You can run other gulp tasks made available via `gulpfile.js`, such as `gulp build:html` or `gulp clean`, to only build/clean certain parts of your project.

or

**Build from Sublime Text with Sublime Gulp**

1. Add `my-project/` to the Sublime Project
    * In Sublime Text, select `Project > Add folder to project...` from the top menu
    * Navigate to and select `my-project/`
2. Open the command pallete with `Ctrl + Shift + P`
3. Search for and select `Gulp`
    * **Note:** Sublime Gulp should automatically detect the included `gulpfile.js` at the root of the project.
4. Select `build`
    * **Note:** You can select other gulp tasks made available via `gulpfile.js`, such as `build:html` or `clean` to only build/clean certain parts of your project.

## Prototype

Editing your prototype is done by modifying the files in the `src/` directory in your project. Files in this directory are fed into gulp during the `build` tasks and "compiled" to form your website.

**Enabling automatic building (optional)**

To tell gulp to automatically update the `dist/` whenever you make a change to the `src/` you can run the `default` task (which is just a series of `gulp.watch()` methods).

In the command line you can run this task by navigating into `my-project/` and running `gulp` (the default task runs without specifying it's name after the `gulp` command).

In the Sublime Text editor, you can run this task by selecting `Gulp` in the command pallete and then selecting the `default` task.

This task will run and watch for file changes until you kill it, either by typing `Ctrl + C` in the command line (or closing the command window), or by running `Gulp: Kill running tasks` in Sublime's command pallette.

**Adding a new page**

1. Navigate to `my-project/src/`
2. Add a new folder, `about/`
3. In the new `about/` folder, add `index.html`
4. Add the below code to `about/index.html`. The `@@include` directives tell gulp-file-include to render the `_header.html` and `_footer.html` partials in the "compiled" version: `my-projdect/dist/about/index.html`

~~~
@@include('includes/_header.html', {"pageTitle": "About"})

<p>Hello, world</p>

@@include('includes/_footer.html')
~~~

**Modifying gulp tasks**

If you want to change the way gulp runs, you can modify the `gulpfile.js` according to the [gulp api](https://github.com/gulpjs/gulp/blob/master/docs/API.md).
